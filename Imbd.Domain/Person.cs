﻿﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imbd.Domain
{
    public class Person
    {
        public DateTime DateOfBirth { get; set; }

        public string Name { get; set; }
    }
}
