﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imbd.Domain
{
    public class Movie
    {
        public string Name { get; set; }

        public string Plot { get; set; }

        public DateTime ReleaseDate { get; set; }

        public Person ProducerOfMovie { get; set; }

        public List<Person> Actors { get; set; }

    }
}
