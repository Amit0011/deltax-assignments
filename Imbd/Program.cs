﻿using System;
using System.Collections.Generic;
using Imbd.Domain;

namespace Imbd
{
    public class Program
    {
        enum ActorProducer
        {
            IsActor = 1,
            IsProducer = 2
        }

        static void Main(string[] args)
        {

            string dateTime, name, plot, producerIndex, actorIndexes, movieIndex;
            int choice;

            ImbdService Imbdobj = new ImbdService();

            var flag = true;

            while (flag)
            {
                Console.WriteLine("Enter the option");
                Console.WriteLine("1.List Movies");
                Console.WriteLine("2.Add Movie");
                Console.WriteLine("3.Add Actor");
                Console.WriteLine("4.Add Producer");
                Console.WriteLine("5.Delete Movie");
                Console.WriteLine("6.Exit");


                Console.WriteLine("Enter choice:");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("enter a number");
                }
                switch (choice)
                {

                    case 1:
                        try
                        {
                            var currLists = Imbdobj.GetMovies();
                            PrintMovieList(currLists);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected Choice : {0} ", e.Message);
                        }
                        break;


                    case 2:
                        try
                        {
                            Console.WriteLine("Enter the movie name : ");
                            name = Console.ReadLine();

                            Console.WriteLine("Enter the release date of movie ");
                            dateTime = Console.ReadLine();

                            Console.WriteLine("Enter the movie plot : ");
                            plot = Console.ReadLine();

                            var currListProducer = Imbdobj.GetProducers();
                            PrintProducerList(currListProducer);
                            Console.WriteLine("Enter the producer index ", currListProducer.Count, " : ");
                            producerIndex = Console.ReadLine();

                            var currListActor = Imbdobj.GetActors();
                            PrintActorList(currListActor);
                            Console.WriteLine("Select all the actors which you want to select by giving comma ", currListActor.Count, " : ");
                            actorIndexes = Console.ReadLine();

                            Imbdobj.AddingMovie(name, dateTime, plot, producerIndex, actorIndexes);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected Choice : {0} ", e.Message);
                        }

                        break;


                    case 3:
                        try
                        {
                            Console.WriteLine("Enter the actor name : ");
                            name = Console.ReadLine();

                            Console.WriteLine("Enter the date of birth of actor in format dd/mm/yyyy: ");
                            dateTime = Console.ReadLine();

                            Imbdobj.AddingPerson(name, dateTime, (int)ActorProducer.IsActor);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected Choice : {0} ", e.Message);
                        }

                        break;



                    case 4:
                        try
                        {
                            Console.WriteLine("Enter the producer name : ");
                            name = Console.ReadLine();

                            Console.WriteLine("Enter the date of birth of producer in format dd/mm/yyyy: ");
                            dateTime = Console.ReadLine();

                            Imbdobj.AddingPerson(name, dateTime, (int)ActorProducer.IsProducer);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected Choice  ", e.Message);
                        }

                        break;


                    case 5:
                        try
                        {
                            var currLists1 = Imbdobj.GetMovies();
                            PrintMovieList(currLists1);
                            Console.WriteLine("Enter the index of the movie");
                            movieIndex = Console.ReadLine();

                            Imbdobj.DeleteMovie(movieIndex);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unexpected Choice : {0} ", e.Message);
                        }

                        break;


                    case 6:
                        flag = false;
                        break;

                    default:
                        Console.WriteLine("Wrong choice entered choose again");
                        break;
                }
            }
        }

        public static void PrintMovieList(List<Movie> currLists)
        {
            foreach (Movie movie in currLists)
            {
                Console.WriteLine("Movie Year of Release : ", movie.ReleaseDate.ToString(), "  ");
                Console.WriteLine(movie.Plot);
                Console.WriteLine("List of Actors ");
                Console.WriteLine("Name of the Producer is : ", movie.ProducerOfMovie.Name);
                Console.WriteLine();
            }
        }

        public static void PrintActorList(List<Person> currListActor)
        {
            Console.WriteLine("Displaying list of actors");

            for (int i = 0; i < currListActor.Count; i++)
            {
                Console.WriteLine(currListActor[i].Name, " ", currListActor[i].DateOfBirth);
            }
        }

        public static void PrintProducerList(List<Person> currListProducer)
        {
            Console.WriteLine("Displaying the list of producers");

            for (int i = 0; i < currListProducer.Count; i++)
            {
                Console.WriteLine(currListProducer[i].Name, " ", currListProducer[i].DateOfBirth);
            }
        }

    }
}
