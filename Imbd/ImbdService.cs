﻿ using System;
using System.Collections.Generic;
using System.Linq;
using Imbd.Domain;
using Imbd.Repository;
namespace Imbd
{
    public class ImbdService
    {
        private ActorRepository _actorRepository;

        private MovieRepository _movieRepository;

        private ProducerRepository _producerRepository;

        public ImbdService()
        {
            _actorRepository = new ActorRepository();
            _movieRepository = new MovieRepository();
            _producerRepository = new ProducerRepository();
        }

        public void AddingPerson(string name, string dateTime, int check)
        {
            DateTime date;
            if (!DateTime.TryParse(dateTime, out date))
            {
                throw new ArgumentException("Wrong date format");
            }
            Person person = new Person()
            {
                DateOfBirth = date,
                Name = name
            };
            if (check == 1)
                _actorRepository.Add(person);
            else
                _producerRepository.Add(person);
        }



        public List<Person> GetActors()
        {
            if (_actorRepository.Get().Count == 0)
                throw new ArgumentException("Actors List is Empty first Add some actors");
            return _actorRepository.Get();
        }


        
        public List<Person> GetProducers()
        {
            if (_producerRepository.Get().Count == 0)
                throw new ArgumentException("Producers List is Empty first Add some producers");
            return _producerRepository.Get();
        }

       
        public List<Movie> GetMovies()
        {
            if (_movieRepository.Get().Count == 0)
                throw new ArgumentException("Movie List is Empty first Add some Movies");
            return _movieRepository.Get();
        }


       

        public void AddingMovie(string name, string dateTime, string plot, string producerIndex, string actorIndexes)
        {
            if (!DateTime.TryParse(dateTime, out DateTime date))
            {
                throw new ArgumentException("Wrong date format entered");
            }
            if(string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("No name found");
            }
            if(string.IsNullOrEmpty(plot))
            {
                throw new ArgumentNullException("No plot found");
            }

            var currListProducer = GetProducers();
            if (!int.TryParse(producerIndex, out int index))
                { 
                throw new ArgumentException("Wrong Input");
            }
            Person producer;
            try
            {
                producer = currListProducer[index - 1];
            }
            catch(Exception e)
            {
                throw new IndexOutOfRangeException("Out of bounds");

            }

            var currListActor = GetActors();
            var actorToAdd = new List<Person>();

            Func<string, int?> tryParse = s => int.TryParse(s, out int n) ? (int?)n : null;
            List<int> indexes = actorIndexes.Split(',')
                                            .Select(s => tryParse(s))
                                            .Where(n => n.HasValue)
                                            .Select(n => n.Value)
                                            .ToList();
            for (int i = 0; i < indexes.Count; i++)
                actorToAdd.Add(currListActor[indexes[i] - 1]);

            Movie movie = new Movie()
            {
                Name = name,
                Plot = plot,
                ReleaseDate = date,
                ProducerOfMovie = producer,
                Actors = actorToAdd
            };

            _movieRepository.Add(movie);
        }
        public void DeleteMovie(string movieIndex)
        {

            int index;
            if (!int.TryParse(movieIndex, out index) || index < 1 || index > GetMovies().Count)
                throw new ArgumentException("Enter a valid index to be deleted");

            _movieRepository.Remove(index - 1);
        }
    }
  

}

