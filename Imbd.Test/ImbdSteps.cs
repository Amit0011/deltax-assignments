﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using System.Linq;
using System.Collections.Generic;
using Imbd.Domain;

namespace Imbd.Tests
{
    [Binding]
    public class ImbdSteps
    {
        private string dateTime,name,plot,producerIndex,actorIndexes,movieIndex;


        List<Movie> movieList;

        ImbdService _imbdService = new ImbdService();

        [Given(@"the MovieName is ""(.*)""")]
        public void GivenTheMovieNameIs(string input)
        {
            name = input;
        }

        [Given(@"the YearOfRelease is ""(.*)""")]
        public void GivenTheYearOfReleaseIs(string input)
        {
    dateTime = input;
            }

        [Given(@"the Plot is ""(.*)""")]
        public void GivenThePlotIs(string input)
        {
    plot = input;
            }

        [Given(@"the indexOfProducer is ""(.*)""")]
        public void GivenTheIndexOfProducerIs(string input)
        {
    producerIndex = input;
            }

        [Given(@"the indexesOfActor are ""(.*)""")]
        public void GivenTheIndexesOfActorAre(string input)
        {
    actorIndexes = input;
            }

        [When(@"adding the movie")]
        public void WhenAddingTheMovie()
        {
    _imbdService.AddingMovie(name, dateTime, plot, producerIndex, actorIndexes);
            }

        [Then(@"the list should look like this")]
        public void ThenTheListShouldLookLikeThis(Table table)
        {
    table.CompareToSet(_imbdService.GetMovies());
            }

        [Then(@"the actor list should look like")]
        public void ThenTheActorListShouldLookLike(Table table)
        {
    table.CompareToSet(_imbdService.GetMovies()[0].Actors);
            }

        [Then(@"the producer should look like this")]
        public void ThenTheProducerShouldLookLikeThis(Table table)
        {
    table.CompareToInstance(_imbdService.GetMovies()[0].ProducerOfMovie);
            }

        [Given(@"get the list of movies")]
        public void GivenGetTheListOfMovies()
        {
                //get the list
            }

        [When(@"fetch the list of movies")]
        public void WhenFetchTheListOfMovies()
        {
    movieList = _imbdService.GetMovies();
            }

        [Then(@"the list after fetching should look like")]
        public void ThenTheListAfterFetchingShouldLookLike(Table table)
        {
    table.CompareToSet(movieList);
            }

        [BeforeScenario("traversal")]

void BeforeFunction1()
        { 
    _imbdService.AddingPerson("Yash", "12/11/1999", 1);
    _imbdService.AddingPerson("Sanjay", "12/14/1990", 1);
    _imbdService.AddingPerson("Vijay", "12/11/1988", 2);
    _imbdService.AddingMovie("KGF", "10/9/2019", "An action movie", "1", "1,2");
    _imbdService.AddingMovie("KGF2", "10/4/2021", "A sequel movie of KGF", "1", "1,2");
            }
    }
}