﻿Feature: Imbd
	full application of imbd
@emptylist
Scenario: Add a movie
	Given the MovieName is "KGF"
	And the YearOfRelease is "10/9/2019"
	And the Plot is "An action movie"
	And the indexOfProducer is "1"
	And the indexesOfActor are "1,2"
    When adding the movie 
	Then the list should look like this
	| MovieName | MovieReleaseDate | MoviePlot       |
	| KGF       | 10/9/2019     | An action movie |
	And the actor list should look like 
	| Name   | DateOfBirth |
	| Yash   | 12/11/1999  |
	| Sanjay | 12/14/1990  |
	And the producer should look like this
	| Name  | DateOfBirth |
	| Vijay | 12/11/1988 |

@traversal
Scenario: List all the movies
Given get the list of movies
When fetch the list of movies
Then the list after fetching should look like
| MovieName | MovieReleaseDate | MoviePlot             |
| KGF       | 10/9/2019        | An action movie       |
| KGF2      | 10/4/2021        | A sequel movie of KGF |
And the actor list should look like 
	| Name   | DateOfBirth |
	| Yash   | 12/11/1999  |
	| Sanjay | 12/14/1990  |
And the producer should look like this
	| Name  | DateOfBirth |
	| Vijay | 12/11/1988  |