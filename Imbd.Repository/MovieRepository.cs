﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imbd.Domain;

namespace Imbd.Repository
{
    public class MovieRepository
    {
        private readonly List<Movie> _movieList;

        public MovieRepository()
        {
            _movieList = new List<Movie>();
        }
        public void Add(Movie movie)
        {
            _movieList.Add(movie);
        }

        public void Remove(int index)
        {
            _movieList.RemoveAt(index - 1);
        }

        public List<Movie> Get()
        {
            return _movieList.ToList();
        }
    }
}