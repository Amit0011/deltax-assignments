﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imbd.Domain;

namespace Imbd.Repository
{
    public class ActorRepository
    {
        private readonly List<Person> _actorList;

        public ActorRepository()
        {
            _actorList = new List<Person>();
        }
        public void Add(Person actor)
        {
            _actorList.Add(actor);
        }

        public List<Person> Get()
        {
            return _actorList.ToList();
        }
    }
}