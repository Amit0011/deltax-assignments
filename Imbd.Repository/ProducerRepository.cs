﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imbd.Domain;

namespace Imbd.Repository
{
    public class ProducerRepository
    {
        private readonly List<Person> _producerList;

        public ProducerRepository()
        {
            _producerList = new List<Person>();
        }
        public void Add(Person producer)
        {
            _producerList.Add(producer);
        }

        public List<Person> Get()
        {
            return _producerList.ToList();
        }
    }
}
